from datetime import datetime

from flask_login import UserMixin

from app import db


class Visitor(db.Model):
    __tablename__ = 'visitor'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    phone = db.Column(db.String, unique=True)
    createdOn = db.Column(db.DateTime, default=datetime.now())
    restaurant_id = db.Column(db.Integer(), db.ForeignKey('restaurant.id', ondelete='CASCADE'))


class Visit(db.Model):
    __tablename__ = 'visit'
    id = db.Column(db.Integer, primary_key=True)
    visitor_id = db.Column(db.Integer, db.ForeignKey(
        'visitor.id', onupdate='CASCADE', ondelete='CASCADE'))
    visitedTime = db.Column(db.DateTime, default=datetime.now())
    contest_id = db.Column(db.Integer, db.ForeignKey(
        'contest.id', onupdate='CASCADE', ondelete='CASCADE'))






class Contest(db.Model):
    __tablename__ = 'contest'
    id = db.Column(db.Integer, primary_key=True)
    start_time = db.Column(db.DateTime, default=datetime.now())
    end_time = db.Column(db.DateTime)
    winner_id = db.Column(db.Integer)
    restaurant_id = db.Column(db.Integer(), db.ForeignKey('restaurant.id', ondelete='CASCADE'))


class Restaurant(db.Model):
    __tablename__ = 'restaurant'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    address = db.Column(db.String)
    locality = db.Column(db.String)
    pincode = db.Column(db.String)
    restaurant_code=db.Column(db.String)


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String, nullable=False, unique=True)
    password = db.Column(db.String, nullable=False, server_default='')
    phone = db.Column(db.String)
    name = db.Column(db.String, nullable=False, server_default='')
    is_owner = db.Column(db.Boolean(), default=False)


class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(50), unique=True)


class UserRoles(db.Model):
    __tablename__ = 'user_roles'
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id', ondelete='CASCADE'))
    role_id = db.Column(db.Integer(), db.ForeignKey('roles.id', ondelete='CASCADE'))
    restaurant_id = db.Column(db.Integer(), db.ForeignKey('restaurant.id', ondelete='CASCADE'))


class Coupon(db.Model):
    __tablename__ = 'coupon'
    id = db.Column(db.Integer(), primary_key=True)
    code = db.Column(db.String)
    owner_mail = db.Column(db.String)
    is_activated = db.Column(db.Boolean(), default=False)


class Config(db.Model):
    __tablename__ = 'config'
    id = db.Column(db.Integer, primary_key=True)
    config_name = db.Column(db.String)
    config_value = db.Column(db.Integer)
    # restaurant_id = db.Column(db.Integer, db.ForeignKey(
    #     'restaurant.id', onupdate='CASCADE', ondelete='CASCADE'))