import os


class Config(object):
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    # SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:postgres@localhost:5433/raffle'
    SQLALCHEMY_TRACK_MODIFICATIONS = True


class DevelopmentConfig(object):
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    SQLALCHEMY_TRACK_MODIFICATIONS = True


config_dict = {
    'debug': Config,
    'development': DevelopmentConfig
}
# class ProductionConfig(object):
#     SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:postgres@localhost:5433/raffle'
#     SQLALCHEMY_TRACK_MODIFICATIONS = True
