import os

from app import app
from flasgger import Swagger
from flask_cors import CORS

if __name__ == '__main__':
    port = int(os.environ.get("PORT", 5000))
    environment = os.environ['FLASK_ENV']
    template = {
        "swagger": "2.0",
        "info": {
            "title": "Hourly Raffle",
            "description": "Hourly API Swagger",
            "termsOfService": "http://foxsense.io",
            "version": "0.0.1"
        },
        "securityDefinitions": {
            "Bearer":{
                "type": "apiKey",
                "name": "Authorization",
                "in": "header"
            }
    }

        # "host": "mysite.com",  # overrides localhost:500
    }
    swagger = Swagger(app, template=template)
    CORS(app)
    host = '0.0.0.0'
    if environment == 'debug':
        host = '127.0.0.1'
    app.run(host=host, port=port)
