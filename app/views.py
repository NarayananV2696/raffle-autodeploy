from datetime import datetime
import os
import requests
from flask import request, Blueprint
from flask_cors import cross_origin
from flask_jwt_extended import jwt_required, get_jwt_identity

import app
import constants
from app import db_task, controller
from app.common_utils import format_phone
from app.view_auth import owner_required, super_admin_required, check_access

home_blueprint = Blueprint('', __name__, )


@home_blueprint.route("/contest/register", methods=['POST'])
def add_user():
    """
       Register for the contest
        ---
        tags:
            - Contest
        parameters:
            - in: body
              name: user
              description: Register a user for contest
              schema:
                type: object
                properties:
                  name:
                    type: string
                  phone:
                    type: string
                  otp:
                    type: string
                  restaurant_code:
                    type: string
        responses:
          200:
            description: User registered
            schema:
              properties:
                status:
                  type: string
        """
    user = request.get_json()
    return controller.add_user(user)


@home_blueprint.route("/contest/register/otp", methods=['POST'])
def otp_generate():
    """
         Send otp
          ---
          tags:
              - Contest
          parameters:
              - in: body
                name: user
                description: Send otp for the phone
                schema:
                  type: object
                  properties:
                    phone:
                      type: string
                    restaurant_code:
                      type: string
          responses:
            200:
              description: OTP Sent
              schema:
                properties:
                  status:
                    type: string
          """
    user = request.get_json()
    return controller.generate_otp(user)


@home_blueprint.route("/rafflestart", methods=['GET'])
def start_job():
    db_task.create_contest()
    if (datetime(datetime.now().year, datetime.now().month, datetime.now().day, datetime.now().hour + 1, 0,
                 0) - datetime.now()).total_seconds() > db_task.config_json['MIN_TIME']:
        app.scheduler.add_job(end_contest, 'interval', seconds=3600,
                              next_run_time=datetime(datetime.now().year, datetime.now().month, datetime.now().day,
                                                     datetime.now().hour + 1, 0,
                                                     0))
    else:
        app.scheduler.add_job(end_contest, 'interval', seconds=3600,
                              next_run_time=datetime(datetime.now().year, datetime.now().month, datetime.now().day,
                                                     datetime.now().hour + 2, 0,
                                                     0))
    app.scheduler.start()


@home_blueprint.route("/rafflestop", methods=['GET'])
def stop_job():
    app.scheduler.shutdown()
    flag = request.args.get('winner')
    return db_task.end_contest(bool(flag))


@home_blueprint.route("/config", methods=['PUT'])
def edit_config():
    config = request.get_json()
    return db_task.edit_configs(config)


@home_blueprint.route("/contest/declare", methods=['GET'])
@jwt_required
@check_access
def end_contest():
    """
        End a contest
        ---
        tags:
            - Contest
        security:
           - Bearer: []
        parameters:
            - in: query
              name: winner
              description: True if winner need to be declared(default:True)
              schema:
                type: boolean
            - in: query
              name: start
              description: True if next contest to be started(default:True)
              schema:
                type: boolean
            - in: query
              name: restaurant
              description: Restaurant code
              schema:
                type: string
        responses:
          200:
            description: Contest ended
            content:
                text/plain:
                    schema:
                        type: string
            """
    flag = request.args.get('winner')
    start = request.args.get('start')
    restaurant = request.args.get('restaurant')
    return controller.close_contest(flag, start, restaurant)


@home_blueprint.route("/contests/open", methods=['GET'])
@jwt_required
def get_contests():
    """
           Fetch all contests
            ---
         tags:
            - Contest
         security:
           - Bearer: []
         responses:
               200:
                description: Restaurant list
            """
    user = get_jwt_identity()
    return controller.get_restaurant_contests(user)


def stop_tick():
    app.scheduler.remove_all_jobs()


@home_blueprint.route("/contest", methods=['POST'])
@jwt_required
@check_access
def start_contest():
    """
          Create Contest
           ---
        tags:
           - Contest
        security:
           - Bearer: []
        parameters:
           - in: body
             name: restaurant
             description: Restaurant code to start the contest
             schema:
               type: object
               properties:
                 restaurant:
                   type: string
        responses:
              200:
                description: Contest creation status
                content:
                    text/plain:
                        schema:
                            type: string
           """
    restaurant = request.get_json()
    return controller.create_contest(restaurant['restaurant'])


@home_blueprint.route("/restaurant", methods=['POST'])
@jwt_required
@owner_required
def create_restaurant():
    """
          Create Restaurant
           ---
        tags:
           - Restaurant
        security:
           - Bearer: []
        parameters:
                - in: body
                  name: restaurant
                  description: Create a restaurant
                  schema:
                    type: object
                    properties:
                      name:
                        type: string
                      address:
                        type: string
                      locality:
                        type: string
                      pincode:
                        type: string

        responses:
          200:
            description: Restaurant created
            schema:
              properties:
                status:
                  type: string
           """
    restaurant = request.get_json()
    user = get_jwt_identity()
    return controller.create_restaurant(restaurant, user)


@home_blueprint.route("/restaurant/manager", methods=['POST'])
@jwt_required
@owner_required
def add_manager():
    """
           Add manager to existing restaurants
             ---
          tags:
             - Restaurant
          security:
           - Bearer: []
          parameters:
                  - in: body
                    name: restaurant
                    description: Add manager
                    schema:
                      type: object
                      properties:
                        restaurant:
                          type: string
                        manager_email:
                          type: string
          responses:
            200:
              description: manager added
              schema:
                properties:
                  status:
                    type: string
             """
    request_json = request.get_json()
    user = get_jwt_identity()
    return controller.add_manager(request_json, user)


@home_blueprint.route("/restaurant/<string:restaurant>/manager", methods=['DELETE'])
@jwt_required
@owner_required
def remove_manager(restaurant):
    """
          Remove manager for a restaurant
          ---
          tags:
              - Restaurant
          security:
             - Bearer: []
          parameters:
              - in: query
                name: manager_email
                description: Manager Mail
                schema:
                  type: string
              - in: path
                name: restaurant
                description: Restaurant Code
                schema:
                  type: string
          responses:
            204:
              description: Manager Deleted
      """
    user = get_jwt_identity()
    manager_email = request.args.get('manager_email')
    return controller.remove_manager(user, manager_email, restaurant)


@home_blueprint.route("/coupon", methods=['POST'])
def add_owner_coupon():
    """
           Create Restaurant owner coupon code
             ---
          tags:
             - Misc
          parameters:
                  - in: body
                    name: restaurant
                    description: Create owner code
                    schema:
                      type: object
                      properties:
                        owner_mail:
                          type: string
          responses:
            200:
              description: Owner coupon created
              schema:
                properties:
                  code:
                    type: string
             """
    request_json = request.get_json()
    return controller.add_owner_coupon(request_json)


@home_blueprint.route("/restaurants", methods=['GET'])
@jwt_required
def get_restaurants():
    """
           Fetch all restaurants
            ---
         tags:
            - Restaurant
         security:
           - Bearer: []
         responses:
               200:
                description: Restaurant list
                properties:
                  managed:
                    type: array
                    items:
                        type: object
                        properties:
                            id:
                            type: string
                            name:
                            type: string
                            address:
                            type: string
                  owned:
                    type: array
                    items:
                        type: object
                        properties:
                            id:
                            type: string
                            name:
                            type: string
                            address:
                            type: string
            """
    user = get_jwt_identity()
    return controller.get_restaurants(user)


@home_blueprint.route("/restaurant", methods=['GET'])
@jwt_required
@check_access
def get_restaurant():
    """
        Get Restaurant details
        ---
        tags:
            - Restaurant
        security:
           - Bearer: []
        parameters:
            - in: query
              name: restaurant
              description: Restaurant Code
              schema:
                type: string
        responses:
               200:
                description: Contest List
                properties:
                  open:
                    type: object
                    properties:
                        start_time:
                        type: string
                        participant:
                        type: integer
                  closed:
                    type: array
                    items:
                        type: object
            """
    restaurant = request.args.get('restaurant')
    return controller.get_restaurant_details(restaurant)


@home_blueprint.route("/restaurant/<string:restaurant>/visitors", methods=['GET'])
@jwt_required
@check_access
def get_visitors(restaurant):
    """
        Get Visitor List for time period
        ---
        tags:
            - Restaurant
        security:
           - Bearer: []
        parameters:
            - in: query
              name: from_date
              description: Data format YYYY-MM-DD
              schema:
                type: string
            - in: query
              name: to_date
              description: Data format YYYY-MM-DD
              schema:
                type: string
            - in: path
              name: restaurant
              schema:
                type: string
        responses:
               200:
                description: Visitor
                properties:
                  count:
                    type: string
                  visitor_list:
                    type: array
                    items:
                        type: object
            """
    from_date = request.args.get('from_date')
    to_date = request.args.get('to_date')
    return controller.get_visitors(restaurant, from_date, to_date)


@home_blueprint.route("/restaurant/<string:restaurant>/contests", methods=['GET'])
@jwt_required
@check_access
def get_contests_by_time(restaurant):
    """
        Get contest List for time period
        ---
        tags:
            - Restaurant
        security:
           - Bearer: []
        parameters:
            - in: query
              name: from_date
              description: Data format YYYY-MM-DD
              schema:
                type: string
            - in: query
              name: to_date
              description: Data format YYYY-MM-DD
              schema:
                type: string
            - in: path
              name: restaurant
              schema:
                type: string
        responses:
               200:
                description: Contest
                properties:
                  count:
                    type: string
                  contest_list:
                    type: array
                    items:
                        type: object
            """
    from_date = request.args.get('from_date')
    to_date = request.args.get('to_date')
    return controller.get_contests_by_time(restaurant, from_date, to_date)
# @home_blueprint.route("/contest", methods=['GET']):
# def get_contest():
