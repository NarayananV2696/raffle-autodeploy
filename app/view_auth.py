from datetime import datetime, timedelta
from functools import wraps

from flask import Blueprint, request, jsonify
from flask_jwt_extended import create_access_token, jwt_refresh_token_required, get_jwt_identity, create_refresh_token, \
    verify_jwt_in_request, get_jwt_claims
from app import db, db_task, controller, jwt

user_blueprint = Blueprint('/user', __name__, )


@user_blueprint.route('/login', methods=['POST'])
def login():
    """
           User Login
            ---
            tags:
                - User Auth
            parameters:
                - in: body
                  name: user
                  description: User Login
                  schema:
                    type: object
                    properties:
                      email:
                        type: string
                      password:
                        type: string
            responses:
              200:
                description: User registered
                schema:
                  properties:
                    token:
                      type: string
                    refreshToken:
                      type: string
                    expires_in:
                      type: string
                    token_type:
                      type: string
            """
    user_json = request.get_json()
    return controller.signin_user(user_json)


@user_blueprint.route('/register', methods=['POST'])
def register():
    """
           User registration
            ---
            tags:
                - User Auth
            parameters:
                - in: body
                  name: user
                  description: Register a User
                  schema:
                    type: object
                    properties:
                      name:
                        type: string
                      phone:
                        type: string
                      password:
                        type: string
                      email:
                        type: string
                      coupon:
                        type: string
            responses:
              200:
                description: User registered
                schema:
                  properties:
                    status:
                      type: string
            """
    user_req = request.get_json()
    return controller.register_user(user_req)


@user_blueprint.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    """
               User Login
                ---
                tags:
                    - User Auth
                security:
                    - Bearer: []
                responses:
                  200:
                    description: User registered
                    schema:
                      properties:
                        access_token:
                          type: string
                """
    current_user = get_jwt_identity()
    ret = {
        'access_token': create_access_token(
            identity={'email': current_user['email'], 'is_owner': current_user['is_owner'], 'name': current_user['name']},
            expires_delta=timedelta(days=1))
    }
    return jsonify(ret), 200


@user_blueprint.route('/login/forgot/otp', methods=['POST'])
def forgot_password():
    """
           Forgot Password
            ---
            tags:
                - User Auth
            parameters:
                - in: body
                  name: email
                  description: Generate OTP for account verification
                  schema:
                    type: object
                    properties:
                      email:
                        type: string
            responses:
              200:
                description: User registered
                schema:
                  properties:
                    status:
                      type: string
            """
    user_req = request.get_json()
    return controller.forgot_password(user_req)


@user_blueprint.route('/login/forgot', methods=['POST'])
def forgot_password_otp():
    """
           Forgot Password OTP
            ---
            tags:
                - User Auth
            parameters:
                - in: body
                  name: email
                  description: Send OTP for account verification
                  schema:
                    type: object
                    properties:
                      email:
                        type: string
                      otp:
                        type: string
                      new_password:
                        type: string
            responses:
              200:
                description: User registered
                schema:
                  properties:
                    status:
                      type: string
            """
    user_req = request.get_json()
    return controller.verify_otp_password(user_req)


def owner_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        identity = get_jwt_identity()
        user = db_task.get_user(identity['email'])
        if not user.is_owner:
            return jsonify(msg='Only owners have access!'), 403
        else:
            return fn(*args, **kwargs)
    return wrapper


def check_access(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        identity = get_jwt_identity()
        user = db_task.get_user(identity['email'])
        restaurant = request.args.get('restaurant')
        if not restaurant:
            if 'restaurant' in kwargs.keys():
                restaurant = kwargs['restaurant']
            if not restaurant:
                restaurant = request.get_json()
                restaurant = restaurant['restaurant']
        if db_task.check_access(user, restaurant):
            return fn(*args, **kwargs)
        else:
            return jsonify(msg='Restaurant not accessible'), 403
    return wrapper


def super_admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        email = get_jwt_identity()
        roles = db_task.get_roles(email)
        if 'SUPERADMIN' not in roles:
            return jsonify(msg='Only Super admins have access.'), 403
        else:
            return fn(*args, **kwargs)

    return wrapper
