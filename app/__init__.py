from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
import os
from config import config_dict
from flask_jwt_extended import JWTManager


app = Flask(__name__)
environment = os.environ['FLASK_ENV']
app.config.from_object(config_dict[environment])
app.config['JWT_SECRET_KEY'] = 'efqefwefefwefe'
jwt = JWTManager(app)
db = SQLAlchemy(app)
db.init_app(app)
migrate = Migrate(app, db)
from apscheduler.schedulers.background import BackgroundScheduler
scheduler = BackgroundScheduler()
from app.views import home_blueprint
app.register_blueprint(home_blueprint)
from app.view_auth import user_blueprint
app.register_blueprint(user_blueprint)
from app import db_task
db_task.get_configs()

