def format_phone(phone):
    if len(phone) == 10:
        return '91'+phone
    else:
        return phone


if __name__ == "__main__":
    print(format_phone('1234567890'))

