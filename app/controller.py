import os
import datetime
import random
import requests
from flask_jwt_extended import create_refresh_token, create_access_token
from flask_login import login_user
from werkzeug.security import generate_password_hash, check_password_hash

import constants
from app import db_task
from app.common_utils import format_phone


def add_user(user):
    user['phone'] = format_phone(user['phone'])
    data = {
        "authkey": constants.AUTH_KEY,
        "otp": user['otp'],
        "mobile": user['phone']
    }
    if os.environ['FLASK_ENV'] == 'production':
        req = requests.post('https://api.msg91.com/api/v5/otp/verify', params=data)
        response = req.json()
        if response['type'] == 'success' or response['message'] == constants.ALREADY_VERIFIED:
            return
        else:
            return response, 400
    else:
        if user['otp'] == '123456':
            return db_task.add_visit(user)
        else:
            return {'type': 'OTP Error'}, 400


def generate_otp(user):
    user['phone'] = format_phone(user['phone'])
    data = {
        "authkey": constants.AUTH_KEY,
        "template_id": constants.TEMPLATE_ID,
        "mobile": user['phone'],
        "otp_expiry": 15
    }
    user_profile = db_task.get_visitor_profile(user['phone'], user['restaurant_code'])
    if user_profile and user_profile['current_contest']:
        return {'status': constants.MULTIPLE_ENTRY}
    if os.environ['FLASK_ENV'] == 'production':
        req = requests.get('https://api.msg91.com/api/v5/otp', params=data)
        response = req.json()
        if 'request_id' in response.keys():
            del response['request_id']
    else:
        response = {'type': 'success'}
    response['user'] = user_profile
    return response


def close_contest(flag, start, restaurant):
    if restaurant is None:
        return {'status': 'Restaurant id unavailable'}, 400
    if flag is None or flag == 'True' or flag == 'true':
        flag = True
    else:
        flag = False
    if start is None or start == 'True' or start == 'true':
        start = True
    else:
        start = False
    return db_task.end_contest(flag=flag, start=start, restaurant=restaurant)


def create_contest(restaurant):
    restaurant = db_task.get_restaurant_by_code(restaurant)
    return {'status': db_task.create_contest(restaurant.id)}


def register_user(user_req):
    mandatory = ['email', 'phone', 'password', 'name']
    if all(x in user_req.keys() for x in mandatory):
        if db_task.get_user(user_req['email']):
            return {'status': 'User already exists. Try login.'}, 200
        user_req['password'] = generate_password_hash(user_req['password'])
        return db_task.create_user(user_req)
    else:
        return {'status': 'Credentials Missing'}, 400


def signin_user(user_json):
    if user_json['email'] and user_json['password']:
        user = db_task.get_user(user_json['email'])
        if user:
            if check_password_hash(user.password, user_json['password']):
                refreshToken = create_refresh_token(
                    identity={'email': user.email, 'is_owner': user.is_owner, 'name': user.name},
                    expires_delta=datetime.timedelta(days=7))
                return {'token': create_access_token(
                    identity={'email': user.email, 'is_owner': user.is_owner, 'name': user.name},
                    expires_delta=datetime.timedelta(days=1)),
                    'refreshToken': refreshToken, 'token_type': 'Bearer', 'expires_in': 86400}
            else:
                return {'status': constants.INCORRECT_PASSWORD}, 401
        else:
            return {'status': constants.USER_DOESNOT_EXIST}, 401
    else:
        return {'status': constants.CREDENTIALS_MISSING}, 401


def create_restaurant(restaurant, user):
    key_list = ['name', 'locality']
    rest_code = ""
    for key in key_list:
        label = restaurant[key]
        property_arr = str(label).split(' ')
        if len(property_arr) > 1:
            for label in property_arr:
                rest_code = rest_code + label[0]
        else:
            rest_code = rest_code + label[0] + label[-1]

    rest_code = rest_code + restaurant['pincode'][0] + restaurant['pincode'][-2:]
    rest_code = rest_code + str(0)
    i = 0
    while db_task.check_restaurant_code(rest_code):
        i = i + 1
        rest_code = rest_code[:len(rest_code) - 1] + str(i)
    restaurant['restaurant_code'] = rest_code
    return db_task.add_restaurant(restaurant, user)


def add_manager(request_json, user):
    manager_mail = request_json['manager_email']
    owner = db_task.get_restaurant_owner(request_json['restaurant'])
    if owner.id != db_task.get_user(user['email']).id:
        return {'status': 'Not the owner'}, 401
    if db_task.check_user(manager_mail):
        return db_task.add_manager(request_json)
    else:
        return {'status': 'Not a User'}, 400


def add_owner_coupon(request_json):
    time_tuple = datetime.datetime.utcnow().timestamp()
    coupon = str(time_tuple).split('.')[1]
    return db_task.create_coupon(request_json, coupon)


def get_restaurants(user):
    roles = db_task.get_roles(user['email'])
    restaurants = db_task.get_restaurants(roles)
    restaurant_json = {}
    for key in restaurants.keys():
        restaurant_json[key] = []
        for restaurant in restaurants[key]:
            open_contest, visitors = db_task.get_open_contest(restaurant.restaurant_code)
            rest_json = {'id': restaurant.id, 'name': restaurant.name, 'address': restaurant.address,
                         'locality': restaurant.locality, 'pincode': restaurant.pincode,
                         'restaurant_code': restaurant.restaurant_code}
            if open_contest:
                rest_json['contest'] = {'start_time': open_contest.start_time, 'participants': len(visitors)}
            restaurant_json[key].append(rest_json)
    for key in restaurant_json.keys():
        restaurant_json[key] = {'count': len(restaurant_json[key]), 'data': restaurant_json[key]}
    return restaurant_json



def forgot_password(data):
    user = db_task.get_user(data['email'])
    if user:
        if os.environ['FLASK_ENV'] == 'production':
            data = {
                "authkey": constants.AUTH_KEY,
                "template_id": constants.TEMPLATE_ID,
                "mobile": user.phone,
                "otp_expiry": 15
            }
            req = requests.get('https://api.msg91.com/api/v5/otp', params=data)
            response = req.json()
            if 'request_id' in response.keys():
                del response['request_id']
        else:
            response = {'type': 'success'}
        return response
    else:
        return {'status': constants.USER_DOESNOT_EXIST}, 400


def verify_otp_password(data):
    user = db_task.get_user(data['email'])
    new_password = generate_password_hash(data['new_password'])
    data = {
        "authkey": constants.AUTH_KEY,
        "otp": data['otp'],
        "mobile": user.phone
    }
    if os.environ['FLASK_ENV'] == 'production':
        req = requests.post('https://api.msg91.com/api/v5/otp/verify', params=data)
        response = req.json()
        if response['type'] == 'success' or response['message'] == constants.ALREADY_VERIFIED:
            return {'status': db_task.change_password(user, new_password)}
        else:
            return response, 400
    else:
        if data['otp'] == '123456':
            return {'status': db_task.change_password(user, new_password)}
        else:
            return {'type': 'OTP Error'}


def get_contests(restaurant, from_date=None, to_date=None):
    contest_json = {}
    contest, visitors = db_task.get_open_contest(restaurant)
    contest_json['closed'] = db_task.get_closed_contest(restaurant, from_date, to_date)
    if contest is not None:
        contest_json['open'] = {'start_time': contest.start_time, 'participants': len(visitors)}
        return {'closed': {'count': len(contest_json['closed']), 'data': contest_json['closed']},
                'open': contest_json['open']}
    else:
        return {'closed': {'count': len(contest_json['closed']), 'data': contest_json['closed']}}


def get_restaurant_contests(user):
    restaurants = get_restaurants(user)
    restaurant_list = []
    for key in restaurants.keys():
        restaurant_list = restaurant_list + restaurants[key]['data']
    restaurant_json = {}
    for item in restaurant_list:
        restaurant_json[item['id']] = item
    return {'restaurants': {'count': len(list(restaurant_json.values())), 'data': list(restaurant_json.values())}}


def remove_manager(user, manager_email, restaurant):
    owner = db_task.get_restaurant_owner(restaurant)
    if owner.id != db_task.get_user(user['email']).id:
        return {'status': 'Not the owner'}, 401
    if db_task.check_user(manager_email):
        return db_task.remove_manager(manager_email, restaurant)
    else:
        return {'status': 'Not a User'}, 400


def get_restaurant_details(restaurant):
    if restaurant is None:
        return {'status': 'Restaurant Code missing'}, 400
    restaurant_info = db_task.get_restaurant_by_code(restaurant)
    if restaurant_info:
        contests = get_contests(restaurant)
        users = db_task.get_restaurant_users(restaurant)
        visitors = db_task.get_visitors_by_restaurant(restaurant)
        return {'id': restaurant_info.id, 'name': restaurant_info.name, 'address': restaurant_info.address,
                'locality': restaurant_info.locality, 'pincode': restaurant_info.pincode,
                'restaurant_code': restaurant_info.restaurant_code, 'contests': contests,
                'users': {'count': len(users), 'data': users},
                'visitors': {'count': len(visitors), 'data': visitors}}
    else:
        return {'status': constants.RESTAURANT_NOT_FOUND}, 400


def get_visitors(restaurant, from_date, to_date):
    if restaurant is None:
        return {'status': 'Restaurant Code missing'}, 400
    if from_date and to_date:
        from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d")
        to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d") + datetime.timedelta(days=1)
    else:
        return {'status': 'Time Period missing'}, 400
    visitors = db_task.get_visitors_by_restaurant(restaurant, from_date=from_date, to_date=to_date)
    return {'visitors':{'count': len(visitors), 'data': visitors}}


def get_contests_by_time(restaurant, from_date, to_date):
    if restaurant is None:
        return {'status': 'Restaurant Code missing'}, 400
    if from_date and to_date:
        from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d")
        to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d") + datetime.timedelta(days=1)
    else:
        return {'status': 'Time Period missing'}, 400
    contests =  get_contests(restaurant, from_date, to_date)
    return {'contests': contests['closed']}


if __name__ == '__main__':
    get_restaurant_contests({'email':'ramprasath@foxsense.io'})
#     print(get_contests(2))
# add_owner_coupon({'owner_mail':'praneshvrp1@gmail.com'})
# print(register_user({'email': 'praneshvrp1@gmail.com', 'password': 'segwrg', 'phone': 'grgwgwg', 'name': 'pranesh',
#                'coupon': '665419'}))
