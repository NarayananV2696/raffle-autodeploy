from flask import jsonify

from datetime import datetime, timedelta
from sqlalchemy import func
import constants
from app import db
import models

config_json = {}


def add_visit(visitor_obj):
    global config_json
    try:
        restaurant = visitor_obj['restaurant_code']
        restaurant = models.Restaurant.query.filter(models.Restaurant.restaurant_code == restaurant).first()
        if restaurant is None:
            return {'status': constants.RESTAURANT_NOT_FOUND}, 400
        restaurant = restaurant.id
        del visitor_obj['restaurant_code']
        del visitor_obj['otp']
        visitor = models.Visitor.query.filter(models.Visitor.phone == visitor_obj['phone']).first()
        if not visitor:
            visitor_obj['createdOn'] = datetime.now()
            visitor = models.Visitor(**visitor_obj)
            db.session.add(visitor)
            db.session.flush()
            db.session.commit()
        contest = models.Contest.query.filter(models.Contest.end_time == None,
                                              models.Contest.restaurant_id == restaurant).first()
        if contest is not None:
            visitor_last_visit = get_visitor_last_visit(visitor.id, restaurant)
            visitor_last_win = get_visitor_last_win(visitor.id, restaurant)
            visitor_current_contest = models.Visit.query.filter(models.Visit.visitor_id == visitor.id,
                                                                models.Visit.contest_id == contest.id).first()
            if visitor_last_visit is None or (
                    datetime.now() - visitor_last_visit.visitedTime).total_seconds() > config_json[
                'VISIT_FREQUENTLY'] or visitor_current_contest is None:
                if visitor_last_win is None or (
                        datetime.now() - visitor_last_win.end_time).total_seconds() > config_json[
                    'COOL_DOWN_TIME']:
                    visit = models.Visit(
                        **{'visitor_id': visitor.id, 'contest_id': contest.id, 'visitedTime': datetime.now()})
                    db.session.add(visit)
                    db.session.commit()
                    return {'status': constants.VISIT_SUCCESS}, 200
                else:
                    return {'status': constants.WON_RECENTLY}, 400
            else:
                return {'status': constants.MULTIPLE_ENTRY}, 400
        else:
            return {'status': constants.NO_OPEN_CONTESTS}, 400
    except Exception as e:
        print('exception', e)
        return False


def get_visitor_last_win(visitor_id, restaurant):
    visitor_last_win = models.Contest.query.filter(models.Contest.winner_id == visitor_id,
                                                   models.Contest.end_time != None,
                                                   models.Contest.restaurant_id == restaurant).order_by(
        models.Contest.end_time.desc()).first()
    # visitor_last_win = models.Winner.query.filter(models.Winner.visitor_id == visitor_id).order_by(
    #     models.Winner.visitedTime.desc()).first()
    return visitor_last_win


def get_visitor_profile(mobile, restaurant):
    restaurant = get_restaurant_by_code(restaurant)
    visitor = models.Visitor.query.filter(models.Visitor.phone == mobile).first()
    if visitor:
        visitor_current_contest = models.Visit.query.join(models.Contest, models.Contest.end_time == None).filter(
            models.Visit.visitor_id == visitor.id,
            models.Contest.restaurant_id == restaurant.id,
            models.Visit.contest_id == models.Contest.id).first()
        visitor_last_visit = get_visitor_last_visit(visitor.id, restaurant.id)
        return {'name': visitor.name, 'last_visit': visitor_last_visit.visitedTime if visitor_last_visit else None,
                'current_contest': True if visitor_current_contest else False}
    else:
        return None


def get_visitor_last_visit(visitor_id, restaurant):
    visitor_last_visit = models.Visit.query.join(models.Contest, models.Contest.id == models.Visit.contest_id).filter(
        models.Visit.visitor_id == visitor_id, models.Contest.restaurant_id == restaurant).order_by(
        models.Visit.visitedTime.desc()).first()
    return visitor_last_visit


def end_contest(flag, start, restaurant):
    global config_json
    restaurant = get_restaurant_by_code(restaurant)
    current_contest = models.Contest.query.filter(models.Contest.end_time == None,
                                                  models.Contest.restaurant_id == restaurant.id).first()
    if current_contest:
        winner = models.Visitor.query.join(models.Visit, models.Visitor.id == models.Visit.visitor_id).filter(
            models.Visit.contest_id == current_contest.id).order_by(func.random()).limit(1).with_entities(
            models.Visitor, models.Visit).first()
        final_winner = None
        if flag is True and winner is not None:
            visitor_last_win = get_visitor_last_win(winner[0].id, restaurant.id)
            if visitor_last_win is None or (
                    winner[1].visitedTime - visitor_last_win.end_time).total_seconds() > config_json['WINNER_WAIT']:
                final_winner = winner
            else:
                alter_winner = models.Visitor.query.join(models.Visit,
                                                         models.Visitor.id == models.Visit.visitor_id).filter(
                    models.Visit.contest_id == current_contest.id).order_by(func.random()).limit(
                    1).with_entities(
                    models.Visitor, models.Visit).first()
                visitor_last_win = get_visitor_last_win(alter_winner[0].id, restaurant.id)
                if visitor_last_win is None or (
                        alter_winner[1].visitedTime - visitor_last_win.end_time).total_seconds() > config_json[
                    'WINNER_WAIT']:
                    final_winner = alter_winner
            current_contest.winner_id = final_winner[0].id
            current_contest.end_time = datetime.now()
            db.session.commit()
            if start:
                create_contest(restaurant.id)
            return {'name': final_winner[0].name, 'phone': final_winner[0].phone, 'contest_id': current_contest.id}
        else:
            current_contest.end_time = datetime.now()
            db.session.commit()
            if start:
                create_contest(restaurant.id)
            if flag is False and winner is None:
                return {'status': constants.NO_USER_CONTEST_CLOSED}, 200
            else:
                return {'status': constants.CONTEST_CLOSED}, 200
    else:
        return {'status': constants.NO_OPEN_CONTESTS}, 400


def check_access(user, restaurant):
    restaurant = get_restaurant_by_code(restaurant)
    if restaurant:
        return models.UserRoles.query.filter(models.UserRoles.user_id == user.id,
                                             models.UserRoles.restaurant_id == restaurant.id).first()
    else:
        return None


def get_configs():
    configs = models.Config.query.all()
    global config_json
    for config in configs:
        config_json[config.config_name] = config.config_value


def get_user(email):
    return models.User.query.filter(models.User.email == email).first()


def update_refresh_token(email, token):
    models.User.query.filter(models.User.email == email).update({'refreshToken': token})
    db.session.commit()


def create_contest(restaurant):
    contest = models.Contest.query.filter(models.Contest.end_time == None,
                                          models.Contest.restaurant_id == restaurant).first()
    if contest:
        return constants.ALREADY_OPEN
    models.Contest.query.filter()
    contest = models.Contest(**{'restaurant_id': restaurant, 'start_time': datetime.now()})
    db.session.add(contest)
    db.session.commit()
    return constants.CONTEST_CREATED


def edit_configs(config):
    try:
        for key in config.keys():
            config_result = models.Config.query.filter(models.Config.config_name == key).first()
            config_result.config_value = config[key]
        db.session.commit()
        return True
    except:
        return False


# def try_popular():
#     print(populartimes.get_populartimes("AIzaSyCY0uWb-2l6XNIpcRPYM_2G9cdMCFX3ldk", "ChIJDzl4MeuTyzsRaLjSrquwWSI"))
#     # print(populartimes.get("AIzaSyBOpQeoPql964N1TpGsuhpY4M9h9dkS3nw", ["bar"], (48.132986, 11.566126),
#     #                        (48.142199, 11.580047)))

# print(populartimes.get("AIzaSyBOpQeoPql964N1TpGsuhpY4M9h9dkS3nw", ["bar"], (13.067439, 80.237617),(13.06839, 80.23617), radius=500))

def create_user(user_req):
    coupon_code = ''
    try:
        if 'coupon' in user_req.keys():
            coupon_code = user_req['coupon']
            del user_req['coupon']
        user = models.User(**user_req)
        db.session.add(user)
        db.session.flush()
        if coupon_code != '':
            coupon = models.Coupon.query.filter(models.Coupon.owner_mail == user_req['email'],
                                                models.Coupon.code == coupon_code).first()
            if coupon:
                user.is_owner = True
                coupon.is_activated = True
                db.session.commit()
                return {'status': constants.OWNER_CREATED}
            else:
                db.session.rollback()
                return {'status': constants.INVALID_COUPON}, 400
        db.session.commit()
        return {'status': constants.USER_CREATED}
    except Exception as e:
        print(e)
        return {'status': constants.USER_CREATION_FAILED}, 400


def get_role_id(role_name):
    role = models.Role.query.filter(models.Role.name == role_name).first()
    if role:
        return role.id


def add_restaurant(restaurant, user):
    try:
        user = get_user(user['email'])
        restaurant_obj = models.Restaurant(**restaurant)
        db.session.add(restaurant_obj)
        db.session.flush()
        user_role = models.UserRoles(
            **{'user_id': user.id, 'role_id': get_role_id('OWNER'), 'restaurant_id': restaurant_obj.id})
        db.session.add(user_role)
        db.session.commit()
        return {'status': constants.RESTAURANT_CREATION_SUCCESSFUL}, 200
    except:
        return {'status': constants.RESTAURANT_CREATION_FAILED}, 400


def get_roles(email):
    user = get_user(email)
    if user:
        owner_roles = models.UserRoles.query.filter(models.UserRoles.user_id == user.id,
                                                    models.UserRoles.role_id == get_role_id('OWNER')).order_by(
            models.UserRoles.restaurant_id).all()
        manager_roles = models.UserRoles.query.filter(models.UserRoles.user_id == user.id,
                                                      models.UserRoles.role_id == get_role_id('MANAGER')).order_by(
            models.UserRoles.restaurant_id).all()
        owner = []
        for role in owner_roles:
            owner.append(role.restaurant_id)
        manager = []
        for role in manager_roles:
            manager.append(role.restaurant_id)
        return {'owner': owner, 'manager': manager}


# def add_admin(request_json):
def check_user(email):
    return models.User.query.filter(models.User.email == email).first()


def add_manager(data):
    try:
        restaurant = get_restaurant_by_code(data['restaurant'])

        user = get_user(data['manager_email'])
        user_role = models.UserRoles.query.filter(models.UserRoles.restaurant_id == restaurant.id,
                                                  models.UserRoles.role_id == get_role_id('MANAGER'),
                                                  models.UserRoles.user_id == user.id).first()

        if user_role and user_role.user_id == user.id:
            return {'status': constants.MANAGER_ADDED}
        else:
            user_role = models.UserRoles(**
                                         {'user_id': user.id, 'restaurant_id': restaurant.id,
                                          'role_id': get_role_id('MANAGER')})
            db.session.add(user_role)
        db.session.commit()
        return {'status': constants.MANAGER_ADDED}
    except Exception as e:
        print(e)
        return {'status': constants.MANAGER_FAILED}, 400


def create_coupon(data, coupon):
    coupon = models.Coupon(**{'code': coupon, 'owner_mail': data['owner_mail']})
    db.session.add(coupon)
    db.session.flush()
    db.session.commit()
    return {'code': coupon.code}


def get_restaurants(roles):
    owner_restaurants = models.Restaurant.query.filter(models.Restaurant.id.in_(roles['owner'])).order_by(
        models.Restaurant.id).all()
    manager_restaurants = models.Restaurant.query.filter(models.Restaurant.id.in_(roles['manager'])).order_by(
        models.Restaurant.id).all()
    return {'owned': list(owner_restaurants),
            'managed': list(manager_restaurants)}


def get_open_contest(restaurant):
    restaurant = get_restaurant_by_code(restaurant)
    contest = models.Contest.query.filter(models.Contest.restaurant_id == restaurant.id,
                                          models.Contest.end_time == None).first()
    visitors = []
    if contest:
        visitors = models.Visit.query.filter(models.Visit.contest_id == contest.id).all()
    return contest, visitors


def get_restaurant_by_code(code):
    return models.Restaurant.query.filter(models.Restaurant.restaurant_code == code).first()


def get_restaurant_owner(restaurant_code):
    restaurant = get_restaurant_by_code(restaurant_code)
    return models.User.query.join(models.UserRoles, models.UserRoles.restaurant_id == restaurant.id).filter(
        models.UserRoles.role_id == get_role_id('OWNER')).first()


def change_password(user, password):
    user_obj = models.User.query.filter(models.User.id == user.id).first()
    user_obj.password = password
    db.session.commit()
    return constants.PASSWORD_CHANGED


def get_visitor_details(visitor_id):
    return models.Visitor.query.filter(models.Visitor.id == visitor_id).first()


def get_closed_contest(restaurant, from_date=None, to_date=None):
    if from_date is None:
        to_date = datetime.utcnow() + timedelta(days=1)
        from_date = datetime(datetime.now().year, datetime.now().month, 1, 0, 0, 0)
    restaurant = get_restaurant_by_code(restaurant)
    contests = models.Contest.query.filter(models.Contest.restaurant_id == restaurant.id,
                                           models.Contest.end_time != None).filter(
        models.Contest.start_time > from_date).filter(models.Contest.start_time < to_date).order_by(
        models.Contest.start_time.desc()).all()
    contest_list = []
    for contest in contests:
        participants = models.Visit.query.filter(models.Visit.contest_id == contest.id).count()
        contest_json = {'start_time': contest.start_time, 'end_time': contest.end_time, 'participants': participants}
        if contest.winner_id:
            winner = get_visitor_details(contest.winner_id)
            contest_json['winner'] = {'name': winner.name, 'phone': winner.phone}
        contest_list.append(contest_json)
    return contest_list


def get_restaurant_users(restaurant):
    restaurant = get_restaurant_by_code(restaurant)
    results = models.User.query.join(models.UserRoles, models.UserRoles.user_id == models.User.id).join(models.Role,
                                                                                                        models.Role.id == models.UserRoles.role_id).filter(
        models.UserRoles.restaurant_id == restaurant.id).with_entities(models.User, models.UserRoles,
                                                                       models.Role).order_by(models.User.id).all()
    users = {}
    users_list = []
    for result in results:
        if result.User.id in users.keys():
            users[result.User.id]['roles'].append(result.Role.name)
        else:
            users[result.User.id] = {'name': result.User.name, 'phone': result.User.phone, 'email': result.User.email,
                                     'roles': [result.Role.name]}
    for key in users.keys():
        users_list.append(users[key])
    return users_list


def get_visitors_by_restaurant(restaurant, from_date=None, to_date=None):
    if from_date is None:
        to_date = datetime.utcnow() + timedelta(days=1)
        from_date = datetime(datetime.now().year, datetime.now().month, 1, 0, 0, 0)
    visitor_json = {}
    visitor_list = []
    restaurant = get_restaurant_by_code(restaurant)
    results = models.Visitor.query.join(models.Visit, models.Visit.visitor_id == models.Visitor.id).join(
        models.Contest,
        models.Visit.contest_id == models.Contest.id).filter(
        models.Contest.restaurant_id == restaurant.id).filter(models.Visit.visitedTime > from_date).filter(
        models.Visit.visitedTime < to_date).with_entities(models.Visitor, models.Visit,
                                                          models.Contest).order_by(models.Visitor.id).all()

    for result in results:
        if result.Visitor.id in visitor_json.keys():
            visitor_json[result.Visitor.id]['visit_count'] = visitor_json[result.Visitor.id]['visit_count'] + 1
            if result.Contest.winner_id == result.Visitor.id:
                visitor_json[result.Visitor.id]['contest_won'] = visitor_json[result.Visitor.id]['contest_won'] + 1
        else:
            winning_count = 0
            visit_count = 1
            if result.Contest.winner_id == result.Visitor.id:
                winning_count = winning_count + 1
            visitor_json[result.Visitor.id] = {'name': result.Visitor.name, 'phone': result.Visitor.phone,
                                               'visit_count': visit_count, 'contest_won': winning_count}
    for visitor in visitor_json.keys():
        visitor_list.append(visitor_json[visitor])
    return visitor_list


def check_restaurant_code(rest_code):
    restaurant = models.Restaurant.query.filter(models.Restaurant.restaurant_code == rest_code).first()
    if restaurant:
        return True
    else:
        return False


def remove_manager(manager_email, restaurant):
    restaurant = get_restaurant_by_code(restaurant)

    user = get_user(manager_email)
    models.UserRoles.query.filter(models.UserRoles.restaurant_id == restaurant.id,
                                  models.UserRoles.role_id == get_role_id('MANAGER'),
                                  models.UserRoles.user_id == user.id).delete()
    db.session.commit()
    return {}, 204


if __name__ == '__main__':
    # pass
    print(get_visitors_by_restaurant('sgsg156'))
    # try_popular()
    # print(create_contest())
