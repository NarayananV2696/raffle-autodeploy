import os

import xlrd

import models
from app import db


def upload_config():
    models.Config.query.delete()
    workbook = xlrd.open_workbook('config.xlsx')
    worksheet = workbook.sheet_by_index(0)
    for i in range(1, worksheet.nrows):
        config = models.Config(
            **{'config_name': worksheet.cell(i, 0).value, 'config_value': worksheet.cell(i, 1).value})
        db.session.add(config)
        db.session.commit()


def upload_roles():
    models.Role.query.delete()
    workbook = xlrd.open_workbook('config.xlsx')
    worksheet = workbook.sheet_by_index(1)
    for i in range(0, worksheet.nrows):
        role = models.Role(
            **{'name': worksheet.cell(i, 0).value})
        db.session.add(role)
        db.session.commit()


from app.db_task import get_configs
get_configs()
